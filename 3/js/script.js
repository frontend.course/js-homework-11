const drawCircleButton = document.querySelector("#draw-circle");
const wrapper = document.createElement("div");
const circleWrapper = document.createElement("div");

function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

    return `rgb(${r}, ${g}, ${b})`;
}

drawCircleButton.addEventListener("click", function (event) {
    const inputNumber = document.createElement("input");
    inputNumber.type = "number";
    inputNumber.placeholder = "Введіть діаметр кола";

    wrapper.append(inputNumber);

    const startDrawButton = document.createElement("button");
    startDrawButton.innerText = "Намалювати";
    startDrawButton.addEventListener("click", function (event) {
        if (inputNumber.value === "" || inputNumber.value <= 0) {
            alert("Введіть значення більше нуля");
            return;
        }
        wrapper.remove();

        circleWrapper.style.cssText = `
            display: grid;
            grid-template-columns: repeat(10, ${inputNumber.value}px)
        `;

        for (let i = 0; i < 100; i++) {
            const circle = document.createElement("div");

            const backgroundColor = getRandomColor();
            circle.dataset.target = "circle";
            circle.style.cssText = `
                background-color: ${backgroundColor};
                height: ${inputNumber.value}px;
                width: ${inputNumber.value}px;
                border-radius: 50%;
            `;

            circleWrapper.append(circle);
        }
        document.body.append(circleWrapper);
    });

    wrapper.append(startDrawButton);
    this.remove();
    document.body.append(wrapper);
});

circleWrapper.addEventListener("click", function (event) {
    if (event.target.dataset["target"] !== "circle") {
        return;
    }

    event.target.remove();
});
