const priceInput = document.querySelector("#price");
const wrapper = document.createElement("div");
const span = document.createElement("span");
span.style.display = "inline-block";

const removeButton = document.createElement("button");
removeButton.innerText = "X";
removeButton.style.display = "inline-block";
wrapper.append(span);
wrapper.append(removeButton);

priceInput.addEventListener("focus", function (e) {
    this.style.borderColor = "green";
});

const errorSpan = document.createElement("span");
errorSpan.style.display = "block";
errorSpan.innerText = "Please enter correct price";

priceInput.addEventListener("blur", function (e) {
    console.log(this.value);
    if (this.value === "") {
        return;
    }
    resetAdditionalInfo();

    this.style.borderColor = null;
    span.innerText = `Поточна ціна: ${this.value}`;
    if (this.value < 0) {
        this.style.borderColor = "red";
        this.insertAdjacentElement("afterend", errorSpan);
    } else {
        this.insertAdjacentElement("beforebegin", wrapper);
    }
});

function resetAdditionalInfo() {
    wrapper.remove();
    errorSpan.remove();
}

removeButton.addEventListener("click", function (ev) {
    ev.preventDefault();
    priceInput.value = "";
    resetAdditionalInfo();
});
