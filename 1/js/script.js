const passwordForm = document.querySelector("#password-form");

passwordForm.addEventListener("click", function (event) {
    if (!event.target.classList.contains("icon-password")) {
        return;
    }

    const wrapper = event.target.closest(".input-wrapper");
    const input = wrapper.querySelector("input");
    const icon = wrapper.querySelector(".icon-password");

    if (input.type === "password") {
        input.type = "text";
        icon.classList.replace("fa-eye", "fa-eye-slash");
    } else {
        input.type = "password";
        icon.classList.replace("fa-eye-slash", "fa-eye");
    }
});

const errorParagraph = document.createElement("p");
errorParagraph.classList.add("error-text");
errorParagraph.id = "validation-error";

passwordForm.addEventListener("submit", function (event) {
    event.preventDefault();

    let isValid = true;

    const passwordInput = this.querySelector("#password");
    const passwordRepeatInput = this.querySelector("#password-repeat");

    if (passwordInput.value === "" || passwordRepeatInput.value === "") {
        isValid = false;
        errorParagraph.innerText = "Пароль не може бути пустим";
    }

    if (passwordInput.value !== passwordRepeatInput.value) {
        isValid = false;
        errorParagraph.innerText = "Потрібно ввести однакові значення";
    }

    if (!isValid) {
        if (!this.querySelector("#validation-error")) {
            const btn = this.querySelector(".btn");
            btn.insertAdjacentElement("beforebegin", errorParagraph);
        }

        return;
    }

    this.querySelector("#validation-error").remove();
    passwordInput.value = null;
    passwordRepeatInput.value = null;
    const icons = this.querySelectorAll(".icon-password");
    icons.forEach(function (el) {
        el.classList.replace("fa-eye-slash", "fa-eye");
    });

    alert("You are welcome;");
});
