const buttons = document.querySelectorAll(".btn");
const buttonsMap = [];

buttons.forEach(function (el) {
    buttonsMap[el.innerText] = el;
});

window.addEventListener("keydown", function (event) {
    let search = "";
    if (event.key === "Enter") {
        search = event.key;
    } else {
        search = event.code.slice(-1);
    }

    for (let button in buttonsMap) {
        buttonsMap[button].style.backgroundColor = null;
    }

    if (typeof buttonsMap[search] !== "undefined") {
        buttonsMap[search].style.backgroundColor = "blue";
    }
});
